<?php
session_start();

// var_dump($_SESSION);
// exit();

$data=array();
$data['start_url']= 'index.html';

if(!(isset($_SESSION['KI_PWA'])&&$_SESSION['KI_PWA']=="TRUE"))
{
  $data['name']= 'KlickInvite';
  $data['short_name']= 'KlickInvite';
  $data['display']= 'standalone';
  $data['background_color']= '#193158';
  $data['theme_color']= '#24501f';

  $data['icons'][0]['src']= 'assets/images/ki-logo.png';
  $data['icons'][0]['sizes']= '512x512';
  $data['icons'][0]['type']= 'image/png';
}
else
{
  $data['name']= 'KlickInvite - '.$_SESSION['SSID'];
  $data['short_name']= 'KI '.$_SESSION['SSID'];
  $data['display']= 'standalone';
  // $data['background_color']= '#'.$_SESSION['SSID'].$_SESSION['SSID'].'0000';
  // $data['theme_color']= '#d'.$_SESSION['SSID'].'4b39';
  $data['background_color']= '#193158';
  $data['theme_color']= '#24501f';

  $data['icons'][0]['src']= 'assets/images/ki-logo.png';
  $data['icons'][0]['sizes']= '512x512';
  $data['icons'][0]['type']= 'image/png';
}
 echo json_encode($data);
 exit();
?>
